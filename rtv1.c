/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rtv1.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: erodrigu <erodrigu@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/07 18:21:10 by erodrigu          #+#    #+#             */
/*   Updated: 2016/12/01 16:41:44 by erodrigu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

int		main(/*int argc, char **argv*/)
{
	t_env	*env;

//	if (argc != 2)
//		error (2);
	if (4 > 5)
		return (-42);
	if (init_env(&env) == -1)
		return(-1);
	if (init_scene(env) == -1)
		return(1);
	calcul_viewplanestart(env);
	if (trace(env) == -1)
		return(3);
	mlx_loop(env->mlx);
	return(0);
}

// error type
// 1 = init fail
// 2 = erreur param
// 3 = erreur dans le tracer ou calcul

int		trace(t_env *env)
{
	int		x;
	int		y;
	size_t	col;
	t_ray	*ray;

	x = 0;
	ray = (t_ray*)malloc(sizeof(t_ray));
	while (x < WIDTH)
	{
		y = 0;
		while(y < HEIGHT)
		{
			ray_calcul(ray, env, x, y);
			calcul_all(env, ray);
			if (env->cam->size >= 0.0 && env->cam->obj->type == SPHERE)
			{
				col = get_lighting(env, ray);
				mlx_pixel_put(env->mlx, env->win, x, y, col);
			}
			else if (env->cam->size >= 0.0)
			{
				col = get_lighting(env, ray);
				mlx_pixel_put(env->mlx, env->win, x, y, col);
			}
			env->cam->size = -1;
			y++;
		}
		x++;
	}
	return (0);
}

void	calcul_viewplanestart(t_env *env)
{
	env->viewpl.x = env->cam->pos.x + (env->proj->dist * env->cam->dir.x)
		- (env->proj->width / 2.0);
	env->viewpl.y = env->cam->pos.y + (env->proj->dist * env->cam->dir.y)
		- (env->proj->height / 2.0);
	env->viewpl.z = env->cam->pos.z + (env->proj->dist * env->cam->dir.z);
}


void	normalized(t_pos *point)
{
	if (point->x < 0)
		point->x = -point->x;
	if (point->y < 0)
		point->y = -point->y;
	if (point->z < 0)
		point->z = -point->z;
}
