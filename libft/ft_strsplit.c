/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: erodrigu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/02 00:30:04 by erodrigu          #+#    #+#             */
/*   Updated: 2016/01/05 02:34:36 by erodrigu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	**ft_strsplit(char const *s, char c)
{
	size_t		word;
	size_t		i;
	size_t		len;
	char		**tab;
	size_t		nbrword;

	nbrword = ft_nbrword(s, c);
	tab = (char **)malloc(sizeof(char *) * (nbrword + 1));
	i = 0;
	word = 0;
	if (tab == NULL)
		return (tab);
	while (word < nbrword && s[i])
	{
		len = 0;
		while (s && s[i] == c && s[i] != 0)
			i++;
		while (s && s[i + len] != c && s[i + len])
			len++;
		tab[word] = ft_strsub(s, i, len);
		word++;
		i = i + len;
	}
	tab[nbrword] = NULL;
	return (tab);
}
