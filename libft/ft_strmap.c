/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: erodrigu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/01 18:59:53 by erodrigu          #+#    #+#             */
/*   Updated: 2016/08/21 03:39:15 by erodrigu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strmap(char const *s, char (*f)(char))
{
	char	*str;
	int		i;

	i = 0;
	if (s == NULL)
		return (NULL);
	while (s[i])
		i++;
	str = ft_strnew(i);
	if (f == NULL)
		return (NULL);
	i = 0;
	while (s[i] && str != NULL)
	{
		str[i] = f(s[i]);
		i++;
	}
	return (str);
}
