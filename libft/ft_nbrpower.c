/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_nbrpower.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: erodrigu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/14 21:44:08 by erodrigu          #+#    #+#             */
/*   Updated: 2015/12/14 22:08:28 by erodrigu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_nbrpower(int nbr, int power)
{
	int res;

	res = 0;
	if (power < 1)
		res = nbr * ft_nbrpower(nbr, power - 1);
	else if (power == 1)
		return (nbr);
	else
		return (0);
	return (res);
}
