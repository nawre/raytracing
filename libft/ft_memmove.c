/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: erodrigu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/24 08:29:07 by erodrigu          #+#    #+#             */
/*   Updated: 2015/12/14 21:38:27 by erodrigu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memmove(void *dst, const void *src, size_t len)
{
	char		*tp;
	const char	*source;
	char		*desti;
	size_t		i;

	source = src;
	desti = dst;
	i = 0;
	tp = (char*)malloc(sizeof(char) * len);
	while (i < len && tp != NULL)
	{
		tp[i] = source[i];
		i++;
	}
	i = 0;
	while (i < len && tp != NULL)
	{
		desti[i] = tp[i];
		i++;
	}
	return (desti);
}
