/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: erodrigu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/09 16:07:15 by erodrigu          #+#    #+#             */
/*   Updated: 2015/12/11 09:20:45 by erodrigu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static void	ft_delcontent(void *mem, size_t size)
{
	ft_bzero(mem, size);
	free(mem);
}

t_list		*ft_lstmap(t_list *lst, t_list *(*f)(t_list *elem))
{
	t_list	*newelem;
	t_list	*begin;
	t_list	*next;
	void	*del;

	del = &ft_delcontent;
	newelem = f(lst);
	if (newelem == NULL)
		return (NULL);
	begin = newelem;
	while (lst->next != NULL)
	{
		lst = lst->next;
		next = f(lst);
		if (next == NULL)
		{
			ft_lstdel(&begin, del);
			return (NULL);
		}
		newelem->next = next;
		newelem = next;
	}
	return (begin);
}
