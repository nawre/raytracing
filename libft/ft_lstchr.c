/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: erodrigu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/14 21:52:24 by erodrigu          #+#    #+#             */
/*   Updated: 2015/12/14 22:03:54 by erodrigu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list	*ft_lstchr(t_list *lst, void *content)
{
	if (lst == NULL)
		return (NULL);
	while (lst->content != content)
	{
		if (lst->content == content)
			return (lst);
		if (lst->next != NULL)
			return (NULL);
		lst = lst->next;
	}
	return (lst);
}
