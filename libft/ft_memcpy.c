/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: erodrigu <erodrigu@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/24 06:47:41 by erodrigu          #+#    #+#             */
/*   Updated: 2016/11/04 17:55:28 by erodrigu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memcpy(void *dest, const void *src, size_t n)
{
	size_t			i;
	const char		*cpy;
	char			*paste;

	i = 0;
	cpy = src;
	paste = dest;
	while (i < n)
	{
		paste[i] = cpy[i];
		i++;
	}
	return (dest);
}
