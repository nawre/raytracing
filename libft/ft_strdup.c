/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: erodrigu <erodrigu@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/24 02:07:39 by erodrigu          #+#    #+#             */
/*   Updated: 2016/11/04 10:26:11 by erodrigu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strdup(const char *s1)
{
	char	*dest;
	int		i;

	i = 0;
	dest = ft_strnew(ft_strlen(s1));
	while (s1[i] && dest != NULL)
	{
		dest[i] = s1[i];
		i++;
	}
	if (dest == NULL)
		return (NULL);
	else
		return (dest);
}
