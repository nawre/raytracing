/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memalloc.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: erodrigu <erodrigu@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/27 09:55:34 by erodrigu          #+#    #+#             */
/*   Updated: 2016/11/07 11:33:26 by erodrigu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memalloc(size_t size)
{
	char	*str;
	size_t	i;

	str = NULL;
	if (size == 0)
		return (str);
	str = (char *)malloc(sizeof(char) * size);
	i = 0;
	while (i < size)
		str[i++] = 0;
	return (str);
}
