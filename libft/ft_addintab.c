/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_addintab.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: erodrigu <erodrigu@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/11 05:22:36 by erodrigu          #+#    #+#             */
/*   Updated: 2016/11/07 15:10:48 by erodrigu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdio.h>

char	**ft_addintab(char **tab, char *s)
{
	char	**newtab;
	char	**tmp;
	int		i;
	int		ln;

	ln = 0;
	tmp = tab;
	while (tmp && tmp[ln])
		ln++;
	newtab = (char **)malloc(sizeof(char *) * (ln + 2));
	newtab[ln + 1] = NULL;
	if (!newtab)
		return (NULL);
	i = 0;
	while (i < ln)
	{
		newtab[i] = *tmp;
		tmp++;
		i++;
	}
	newtab[i] = s;
	free(tab);
	return (newtab);
}
