/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   matrice.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: erodrigu <erodrigu@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/25 05:00:42 by erodrigu          #+#    #+#             */
/*   Updated: 2016/12/21 18:04:00 by erodrigu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

t_pos	matrice_mul3x1(t_pos a, double b)
{
	     t_pos	result;

	result.x = a.x * b;
	result.y = a.y * b;
	result.z = a.z * b;
	return (result);
}

t_pos	matrice_mul3x3(t_pos a, t_pos b)
{
	t_pos result;

	result.x = a.x * b.x;
	result.y = a.y * b.y;
	result.z = a.z * b.z;
	return (result);
}

t_pos	matrice_div3x3(t_pos a, t_pos b)
{
	t_pos result;

	result.x = a.x / b.x;
	result.y = a.y / b.y;
	result.z = a.z / b.z;
	return (result);
}

t_pos	matrice_add3x3(t_pos a, t_pos b)
{
	t_pos	result;

	result.x = a.x + b.x;
	result.y = a.y + b.y;
	result.z = a.z + b.z;
	return (result);
}

void	matrice_sub3x1(t_pos *a, double b)
{
	a->x = a->x - b;
	a->y = a->y - b;
	a->z = a->z - b;
}

t_pos	matrice_sub3x3(t_pos a, t_pos b)
{
	t_pos	result;

	result.x = a.x - b.x;
	result.y = a.y - b.y;
	result.z = a.z - b.z;
	return (result);
}

t_pos	matrice_div3x1(t_pos a, double b)
{
	t_pos	result;

	result.x = a.x / b;
	result.y = a.y / b;
	result.z = a.z / b;
	return (result);
}

double	matrice_scalaire(t_pos a, t_pos b)
{
	double	result;

	result = a.x * b.x + a.y * b.y + a.z * b.z;
	return (result);
}


/*
t_pos	matrice_rotate(t_pos a, t_pos rot)
{
	t_pos	result;

	result.x = a.x + cos(rot.y) * a.x + sin(rot.y) + cos(rot.z) * a.x - sin(rot.z) * a.x;
	result.y = a.y + ;
	result.z = a.z + ;
}
*/
double	vector_norme(t_pos *a)
{
	double	result;

	result = a->x * a->x;
	result += a->y * a->y;
	result += a->z * a->z;
	return (sqrt(result));
}
