/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   geoform.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: erodrigu <erodrigu@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/25 09:31:42 by erodrigu          #+#    #+#             */
/*   Updated: 2016/11/29 06:06:19 by erodrigu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

double	calcul_plan(t_obj *pt, t_ray *ray)
{
	return (-(((pt->norm.x * (ray->pos.x - pt->pos.x)) + (pt->norm.y *
		(ray->pos.y - pt->pos.y)) + (pt->norm.z * (ray->pos.z - pt->pos.z)))
		/ (pt->norm.x * ray->dir.x + pt->norm.y * ray->dir.y +
		pt->norm.z * ray->dir.z)));
}
