/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   color.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: erodrigu <erodrigu@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/25 04:54:12 by erodrigu          #+#    #+#             */
/*   Updated: 2017/02/09 16:41:11 by erodrigu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

size_t	twocolor_lerp(unsigned a, unsigned b, const float pc)
{
	t_pos	color;

	if (pc <= 0.0f)
		return (a);
	if (pc >= 1.0f)
		return (b);
	color.x = (float)(a & 0xff0000) * (1.0f - pc) + (float)(b & 0xff0000) * pc;
	color.y = (float)(a & 0x00ff00) * (1.0f - pc) + (float)(b & 0x00ff00) * pc;
	color.z = (float)(a & 0x0000ff) * (1.0f - pc) + (float)(b & 0x0000ff) * pc;
	return((((unsigned)color.x) & 0xff0000) |
			(((unsigned)color.y) & 0x00ff00) |
			(((unsigned)color.z) & 0x0000ff));
}

size_t	calcul_rendu_light(t_pos light, t_env *env)
{
	t_pos color;

	color.x = ((unsigned)(light.x / (env->light->nbrlight + 1)) << 16) * env->cam->obj->diff.x;
	color.y = ((unsigned)(light.y / (env->light->nbrlight + 1)) << 8) * env->cam->obj->diff.y;
	color.z = (unsigned)(light.z / (env->light->nbrlight + 1)) * env->cam->obj->diff.z;
	return((((unsigned)color.x) & 0xff0000) |
		(((unsigned)color.y) & 0x00ff00) |
		(((unsigned)color.z) & 0x0000ff));

}

t_pos	add_int_tpos(t_pos a, int b)
{
	a.x = a.x + (double)(b & 0xFF0000);
	a.y = a.y + (double)(b & 0x00FF00);
	a.z = a.z + (double)(b & 0x0000FF);
	return (a);
}

size_t	onecolor_lerp(unsigned a, const float pc)
{
	t_pos	color;
	int		b;

	b = (a >> 1) & 8355711;
	if (pc <= 0.0f)
		return (a);
	if (pc >= 1.0f)
		return (b);
	color.x = (float)(a & 0xff0000) * (1.0f - pc) + (float)(b & 0xff0000) * pc;
	color.y = (float)(a & 0x00ff00) * (1.0f - pc) + (float)(b & 0x00ff00) * pc;
	color.z = (float)(a & 0x0000ff) * (1.0f - pc) + (float)(b & 0x0000ff) * pc;
	return((((unsigned)color.x) & 0xff0000) |
			(((unsigned)color.y) & 0x00ff00) |
			(((unsigned)color.z) & 0x0000ff));
}

t_pos	set_color(int color)
{
	t_pos	col;

	col.x = (color & 0xff0000) / (float)16711680;
	col.y = (color & 0x00ff00) / (float)65280;
	col.z = (color & 0x0000ff) / (float)255;
	return (col);
}
