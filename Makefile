# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: erodrigu <erodrigu@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2016/11/08 10:28:19 by erodrigu          #+#    #+#              #
#    Updated: 2017/01/03 11:59:11 by erodrigu         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = RTV1
SRC = rtv1.c \
init.c \
color.c \
calcul.c \
geoform.c \
light.c \
matrice.c \

INC = libft
LIB = ./libft/libft.a
LIBDIR = libft
MLX = minilibx_macos
OBJ = $(SRC:.c=.o)
CC = gcc
FLAG = -Wall -Wextra -Werror -o3 -g3 -fsanitize=address
FRAME = -framework OpenGL -framework AppKit

all: $(NAME)

%.o:%.c
	@$(CC) $(FLAG) -c $< -o $@ -I $(INC)

$(NAME): $(LIB) $(OBJ)
	@$(CC) $(FLAG) -o $(NAME) $(OBJ) -L $(MLX) -L $(LIBDIR) -lft -lmlx $(FRAME) && echo "\033[1;4;32m\n>>>>>>>_<<<<<<<\n RTV1 Compilé  \n>>>>>>>_<<<<<<<\n"

$(LIB):
	@make mkcln -C $(LIBDIR) && make -C $(MLX)
clean:
	@rm -rf $(OBJ)

fclean: clean
	@rm -rf $(NAME)

re: fclean all
	@make clean
