/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   calcul.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: erodrigu <erodrigu@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/25 04:57:59 by erodrigu          #+#    #+#             */
/*   Updated: 2016/12/15 16:26:25 by erodrigu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

void	calcul_all(t_env *env, t_ray *ray)
{
	t_obj	*pt;
	double	size;
	char	isset;

	isset = 0;
	pt = env->obj;
	while (pt)
	{
		if (pt->type == SPHERE)
			size = calcul_sphere(pt, ray);
		if (pt->type == PLAN)
			size = calcul_plan(pt, ray);
		if (pt->type == CYLINDRE)
			size = calcul_cylindre(pt, ray);
		if (pt->type == CONE)
			size = calcul_cone(pt, ray);
		if (size > 0 && (size < env->cam->size || !isset))
		{
			isset = 1;
			env->cam->size = size;
			env->cam->obj = pt;
		}
		pt = pt->next;
	}
}

void	ray_calcul(t_ray *ray, t_env *env, int x, int y)
{
	ray->pos.x = env->cam->pos.x;
	ray->pos.y = env->cam->pos.y;
	ray->pos.z = env->cam->pos.z;
	ray->dir.x = (env->viewpl.x + (x * env->proj->xind)) - ray->pos.x;
	ray->dir.y = (env->viewpl.y + (y * env->proj->yind)) - ray->pos.y;
	ray->dir.z = env->viewpl.z - ray->pos.z;
}

double	calcul_sphere(t_obj *pt, t_ray *ray)
{
	double	a;
	double	b;
	double	c;

	a = ray->dir.x * ray->dir.x + ray->dir.y * ray->dir.y
		+ ray->dir.z * ray->dir.z;
	b = 2 * (ray->dir.x * (ray->pos.x - pt->pos.x) +
		ray->dir.y * (ray->pos.y - pt->pos.y) +
		ray->dir.z * (ray->pos.z - pt->pos.z));
	c = ((ray->pos.x - pt->pos.x) * (ray->pos.x -
		pt->pos.x) + (ray->pos.y - pt->pos.y) *
		(ray->pos.y - pt->pos.y) + (ray->pos.z -
		pt->pos.z) * (ray->pos.z - pt->pos.z) -
		pt->radius * pt->radius);
	 return (calcul_det(a, b, c));
}

double	calcul_cone(t_obj *pt, t_ray *ray)
{
	double		a;
	double		b;
	double		c;

	a = ray->dir.x * ray->dir.x + ray->dir.z *
	ray->dir.z - ray->dir.y * ray->dir.y;
	b = 2 * (ray->dir.x * (ray->pos.x - pt->pos.x) +
	ray->dir.z * (ray->pos.z - pt->pos.z) - ray->dir.y *
	(ray->pos.y - pt->pos.y));
	c = ((ray->pos.x - pt->pos.x) * (ray->pos.x - pt->pos.x) +
	(ray->pos.z - pt->pos.z) * (ray->pos.z - pt->pos.z) -
	(ray->pos.y - pt->pos.y) * (ray->pos.y - pt->pos.y));
	return (calcul_det(a, b, c));
}

double	calcul_cylindre(t_obj *pt, t_ray *ray)
{
	double		a;
	double		b;
	double		c;

	a = ray->dir.x * ray->dir.x + ray->dir.z * ray->dir.z;
	b = 2 * (ray->dir.x * (ray->pos.x - pt->pos.x) +
	ray->dir.z * (ray->pos.z - pt->pos.z));
	c = ((ray->pos.x - pt->pos.x) * (ray->pos.x - pt->pos.x) +
	(ray->pos.z - pt->pos.z) * (ray->pos.z - pt->pos.z)) -
	pt->radius * pt->radius;
	return (calcul_det(a, b, c));
}

double	calcul_det(double a, double b, double c)
{
	double det;
	double t1;
	double t2;

	det = b * b - 4.0 * a * c;
	if (det < 0)
		return (-1);
	t1 = (-b + sqrt(det)) / (2.0 * a);
	t2 = (-b - sqrt(det)) / (2.0 * a);
	if (t2 < 0)
		t2 = t1;
	return ((t1 > t2) ? t2 : t1);
}
