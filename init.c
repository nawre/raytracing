/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: erodrigu <erodrigu@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/08 10:39:47 by erodrigu          #+#    #+#             */
/*   Updated: 2017/02/10 13:12:39 by erodrigu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

int		init_env(t_env **env)
{
	*env = (t_env *)malloc(sizeof(t_env));
	if (!(*env))
		return(-1);
	(*env)->mlx = mlx_init();
	if (!(*env)->mlx)
		return (-1);
	(*env)->win = mlx_new_window((*env)->mlx, WIDTH, HEIGHT, "RTV1");
	if (!(*env)->win)
		return (-1);
	(*env)->img_ptr = mlx_new_image((*env)->mlx, WIDTH, HEIGHT);
	if (!(*env)->img_ptr)
		return(-1);
	(*env)->obj = NULL;
	return (0);
}

int		init_scene(t_env *env)
{
	if (init_proj(env) == -1)
		return (-1);
	if (init_cam(env) == -1)
		return (-1);
	if (init_obj(env) == -1)
		return (-1);
	if (init_all_light(env) == -1)
		return (-1);
	return (0);
}

int		init_all_light(t_env *env)
{
	env->light = NULL;
	env->lighta = 75.0;
	// init_light(&(env->light), -100.0, 0.0, 50.0, WHITE);
	init_light(&(env->light), 80.0, 10.0, 50.0, WHITE);
	init_light(&(env->light), 10.0, 10.0, 50.0, YELLOW);
	init_light(&(env->light), 50.0, 1.0, 20.0, BLUE);
	return (0);
}

int		init_light(t_light **light, double x, double y, double z, int color)
{
	t_light	*point;

	point = *light;
	if (!(*light))
	{
		*light = (t_light *)malloc(sizeof(t_light));
		point = *light;
		point->nbrlight = 1;
	}
	else
	{

		while(point->next)
			point = point->next;
		point->next = (t_light *)malloc(sizeof(t_light));
		point = point->next;
		(*light)->nbrlight++;
	}
	if (!point)
		return (-1);
	set_point(&(point->pos), x, y, z);
	point->color = color;
	point->next = NULL;
	return (0);
}

int		init_proj(t_env *env)
{
	env->proj = (t_proj *)malloc(sizeof(t_proj));
	if (!env->proj)
		return (-1);
	env->proj->height = HEIGHTPLANE;
	env->proj->width = WIDTHPLANE;
	env->proj->dist = DISTPLANE;
	env->proj->xind = WIDTHPLANE / (double)WIDTH;
	env->proj->yind = HEIGHTPLANE / (double)HEIGHT;
	return (0);
}

int		init_obj(t_env *env)
{
	if (init_plan(&(env->obj), 0.0, -25.0, 20.0, RED) == -1)
		return (-1);
	if (init_plan(&(env->obj), 20.0, 25.0, 20.0, YELLOW) == -1)
		return (-1);
	if (init_sphere(&(env->obj), 0.0, -20.0, 100.0, 15, RED) == -1)
		return (-1);
	if (init_sphere(&(env->obj), 0.0, 20.0, 100.0, 15, BLUE) == -1)
		return (-1);
	if (init_cylinder(&(env->obj), 0.0, 0.0, 100.0, 10, WHITE) == -1)
		return (-1);
	if (init_sphere(&(env->obj), -10.0, 0.0, 75.0, 5, WHITE) == -1)
		return (-1);
	if (init_sphere(&(env->obj), 10.0, 0.0, 75.0, 5, WHITE) == -1)
		return (-1);
	return (0);
}

int		init_plan(t_obj **obj, double x, double y, double z, int color)
{
	t_obj	*point;
	t_pos	col;

	point = *obj;
	if (!(*obj))
	{
		*obj = (t_obj *)malloc(sizeof(t_obj));
		point = *obj;
	}
	else
	{
		while(point->next)
			point = point->next;
		point->next = (t_obj *)malloc(sizeof(t_obj));
		point = point->next;
	}
	if (!point)
		return (-1);
		set_point(&(point->pos), x, y, z);
		point->type = PLAN;
		col = set_color(color);
		set_point(&(point->norm), 0.0, 1.0, 0.0);
		set_point(&(point->diff), col.x, col.y, col.z);
		point->next = NULL;
		return (0);
}

int		init_cylinder(t_obj **obj, double x, double y, double z, int radius, int color)
{
	t_obj	*point;
	t_pos	col;

	point = add_new_item(obj);
	if (!point)
		return (-1);
	set_point(&(point->pos), x, y, z);
	col = set_color(color);
	set_point(&(point->diff), col.x, col.y, col.z);
	point->type = CYLINDRE;
	point->radius = radius;
	point->shining = 0;
	point->next = NULL;
	return (0);
}

int		init_sphere(t_obj **obj, double x, double y, double z, int radius, int color)
{
	t_obj	*point;
	t_pos	col;

	point = add_new_item(obj);
	if (!point)
		return (-1);
	set_point(&(point->pos), x, y, z);
	col = set_color(color);
	set_point(&(point->diff), col.x, col.y, col.z);
	point->type = SPHERE;
	point->radius = radius;
	point->shining = 0;
	point->shiningn = 2;
	point->next = NULL;
	return (0);
}

t_obj *add_new_item(t_obj **obj)
{
	t_obj	*point;

	point = *obj;
	if (!(*obj))
	{
		*obj = (t_obj *)malloc(sizeof(t_obj));
		return (*obj);
	}
	else
	{
		while(point->next)
			point = point->next;
		point->next = (t_obj *)malloc(sizeof(t_obj));
		return (point->next);
	}
}

int		init_cam(t_env *env)
{
	env->cam = (t_ray *)malloc(sizeof(t_ray));
	if (!env->cam)
		return (-1);
	env->cam->obj = NULL;
	set_point(&(env->cam->pos), 0.0, 0.0, 0.0);
	set_point(&(env->cam->dir), 0.0, 0.0, 1.0);
	env->cam->size = -1.0;
	return (0);
}

void	set_point(t_pos *point, double x, double y, double z)
{
	point->x = x;
	point->y = y;
	point->z = z;
}
