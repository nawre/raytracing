/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   light.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: erodrigu <erodrigu@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/14 11:17:12 by erodrigu          #+#    #+#             */
/*   Updated: 2017/02/14 11:59:58 by erodrigu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */


#include "rtv1.h"

int		get_lighting(t_env *env, t_ray *ray)
{
	t_pos			raylight;
	t_pos			normale;
	t_pos			unit_ray;
	t_pos			obj;
	t_light			*light;

	obj = matrice_mul3x1(ray->dir, env->cam->size);
	normale = matrice_sub3x3(obj, env->cam->obj->pos);
	light = NULL;
	if (env->cam->obj->type == PLAN)
		normale = env->cam->obj->norm;
	while (check_all_light(env, &light) == 1)
	{
		set_point(&(env->light->calcul), env->lighta, env->lighta, env->lighta);
		raylight = matrice_sub3x3(obj, light->pos);
		light->size = vector_norme(&raylight) + PREC;
		unit_ray = matrice_div3x1(raylight, light->size);
		light->is_light = is_light(env->obj, &unit_ray,
			light);
		if (light->is_light)
			is_shining(env, light, &normale, &unit_ray, ray);
		light->is_light = 0;
	}
	return(calcul_rendu_light(env->light->calcul, env));
}

void	is_shining(t_env *env, t_light *light, t_pos *normale, t_pos *unit_ray, t_ray *ray)
{
	t_pos	ref;

	ref = calcul_reflexion(matrice_mul3x1(*unit_ray, -1.0),
		matrice_div3x1(*normale, vector_norme(normale)));
	env->light->calcul = matrice_add3x3(env->light->calcul,
		calcul_light(unit_ray, normale, light));
	if (env->cam->obj->shining > 0.0)
		env->light->calcul = add_int_tpos(env->light->calcul,
		(env->cam->obj->shining * light->color * (pow(fmax(0,
			matrice_scalaire(ref, matrice_mul3x1(ray->dir, 1.0))),
			env->cam->obj->shiningn) - PREC)));
}

t_pos	calcul_reflexion(t_pos unit_ray, t_pos normale)
{
	return (matrice_sub3x3(matrice_mul3x1(normale, 2.0 *
		matrice_scalaire(unit_ray, normale)), unit_ray));
}

int		check_all_light(t_env *env, t_light **light)
{
	if (*light == NULL)
	{
		*light = env->light;
		return (1);
	}
	else
		if ((*light)->next)
		{
			*light = (*light)->next;
			return (1);
		}
	return (0);
}

char	is_light(t_obj *obj, t_pos *dir, t_light *light)
{
	t_obj	*check;
	double	size;
	t_ray	*ray;


	ray = (t_ray *)malloc(sizeof(t_ray));
	ray->pos = light->pos;
	ray->dir = *dir;
	check = obj;
	size = -1;
	while (check)
	{
		if (check->type == SPHERE)
			size = calcul_sphere(check, ray) + PREC;
		if (check->type == PLAN)
			size = calcul_plan(check, ray) + PREC;
		if (check->type == CYLINDRE)
			size = calcul_cylindre(check, ray) + PREC;
		if (size > 0 && size < light->size)
			return (0);
		size = -1;
		check = check->next;
	}
	return (1);
}

t_pos	calcul_light(t_pos *unit_ray, t_pos *normale, t_light *light)
{
	t_pos	unit_normale;
	double	cosinus;
	double	angle;
	double	norme;

	norme = vector_norme(normale);
	unit_normale = matrice_div3x1(*normale, norme);
	cosinus = fabs(get_cos(unit_ray, &unit_normale));
	angle = acos(cosinus);
	if (cosinus <= 0)
		return (calcul_color(0, cosinus));
	else
		return (calcul_color(light->color, cosinus));
}

t_pos	calcul_color(int lightcolor, double angle)
{
	t_pos	light;

	light.x = (double)((lightcolor >> 16) & 0x0000FF) * angle;
	light.y = (double)((lightcolor >> 8) & 0x0000FF) * angle;
	light.z = (double)(lightcolor & 0x0000FF) * angle;
	return (light);
}

double	get_cos(t_pos *a, t_pos *b)
{
	return (a->x * b->x + a->y * b->y + a->z * b->z);
}
