/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rtv1.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: erodrigu <erodrigu@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/07 17:26:22 by erodrigu          #+#    #+#             */
/*   Updated: 2017/01/02 15:01:52 by erodrigu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef RTV1_H
# define RTV1_H

# include "libft.h"
# include "mlx.h"
# include <math.h>
# include <pthread.h>
# include <stdio.h>
# include <unistd.h>
# include <fcntl.h>

# define DISTPLANE 1
# define WIDTHPLANE 1.6
# define HEIGHTPLANE 0.9
# define THREAD 8
# define HEIGHT 900
# define WIDTH 1600
# define SPHERE 1
# define CYLINDRE 2
# define CONE 3
# define PLAN 4
# define BLUE 0x3333FF
# define RED 0xFF0000
# define PURPLE 0xFF22DD
# define YELLOW 0xEEEE22
# define WHITE 0xFFFFFF
# define PREC 0.0000003

typedef	struct	s_pos
{
	double		x;
	double		y;
	double		z;
}				t_pos;

typedef	struct	s_proj
{
	double		height;
	double		width;
	double		dist;
	double		xind;
	double		yind;
}				t_proj;

typedef struct	s_obj
{
	t_pos			pos;
	t_pos			norm;
	t_pos			diff;
	char			type;
	int				radius;
	int				height;
	int				width;
	int				color;
	double			shining;
	int				shiningn;
	struct s_obj	*next;
}				t_obj;

typedef	struct	s_ray
{
	t_pos		pos;
	t_pos		dir;
	double		size;
	t_obj		*obj;
}				t_ray;

typedef struct	s_light
{
	t_pos			pos;
	t_pos			calcul;
	int				color;
	int				nbrlight;
	char			is_light;
	double			size;
	struct	s_light	*next;
}				t_light;

typedef	struct	s_env
{
	void		*mlx;
	void		*win;
	void		*img_ptr;
	char		*img;
	double		lighta;
	t_ray		*cam;
	t_light		*light;
	t_pos		viewpl;
	t_proj		*proj;
	t_obj		*obj;
}				t_env;


/*
tab[10][2]{{1, &calcul_quadri}{2, &calcul_sphere}}

while(i < 9)
{
	if (nbr = tab[i][0])
		obj->func = tab[10][1];
	i++;
}*/

int		trace(t_env *env);
t_pos	matrice_add1x3(t_pos a, t_pos b);
t_pos	matrice_sub3x3(t_pos a, t_pos b);
t_pos	matrice_mul3x3(t_pos a, t_pos b);
t_pos	matrice_mul3x1(t_pos a, double b);
t_pos	matrice_div3x1(t_pos a, double b);
t_pos	matrice_add3x3(t_pos a, t_pos b);
t_pos	matrice_div3x3(t_pos a, t_pos b);
int		init_env(t_env **env);
int		init_scene(t_env *env);
int		init_proj(t_env *env);
int		init_cam(t_env *env);
int		sec_obj(t_env *env);
void	ray_calcul(t_ray *ray, t_env *env, int x, int y);
double	calcul_sphere(t_obj *pt, t_ray *ray);
double	calcul_det(double a, double b, double c);
void	calcul_viewplanestart(t_env *env);
size_t	onecolor_lerp(unsigned int a, const float pc);
size_t	twocolor_lerp(unsigned int a, unsigned int b, const float pc);
void	calcul_all(t_env *env, t_ray *ray);
double	calcul_plan(t_obj *pt, t_ray *ray);
int		init_plan(t_obj **obj, double x, double y, double z, int color);
int		init_sphere(t_obj **obj, double x, double y, double z, int radius, int color);
int		init_obj(t_env *env);
void	set_point(t_pos *point, double x, double y, double z);
int		get_lighting(t_env *env, t_ray *ray);
char	is_light(t_obj *obj, t_pos *dir, t_light *light);
t_pos	calcul_light(t_pos *raylight, t_pos *normale, t_light *light);
double	get_cos(t_pos *a, t_pos *b);
int		init_all_light(t_env *env);
double	vector_norme(t_pos *a);
t_pos	matrice_div3x1(t_pos a, double b);
t_pos	calcul_color(int lightcolor, double angle);
void	matrice_sub3x1(t_pos *a, double b);
t_pos	set_color(int color);
int		check_all_light(t_env *env, t_light **light);
int		init_light(t_light **light, double x, double y, double z, int color);
size_t	calcul_rendu_light(t_pos light, t_env *env);
double	calcul_cone(t_obj *pt, t_ray *ray);
double	calcul_cylindre(t_obj *pt, t_ray *ray);
t_obj 	*add_new_item(t_obj **obj);
int		init_cylinder(t_obj **obj, double x, double y, double z, int radius, int color);
double	matrice_scalaire(t_pos a, t_pos b);
t_pos	add_int_tpos(t_pos a, int b);
void	is_shining(t_env *env, t_light *light, t_pos *normale, t_pos *unit_ray, t_ray *ray);
t_pos	calcul_reflexion(t_pos unit_ray, t_pos normale);

#endif
